import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainClass {

	private static int THRESH = 4;
	private static int K = 10;

	private static int MAX = 10;
	private static int MIN = 10;

	private static String NAME = "SAMPLE";

	public static void main(String[] args) {
		try {

			String[] allWebPages = {
					"https://pl.wikipedia.org/wiki/Acefalia_(biologia)",
					"https://pl.wikipedia.org/wiki/Anamorfoza_(biologia)",
					"https://pl.wikipedia.org/wiki/Archeozoologia",
					"https://pl.wikipedia.org/wiki/Branickii",
					"https://pl.wikipedia.org/wiki/Efekt_%C5%81azarza",

					"https://pl.wikipedia.org/wiki/Mark_Ehrenfried",
					"https://pl.wikipedia.org/wiki/Exsultate,_jubilate",
					"https://pl.wikipedia.org/wiki/Fryderyki_-_fonograficzny_debiut_roku_muzyki_powa%C5%BCnej",
					"https://pl.wikipedia.org/wiki/Jean-Marie_Londeix",
					"https://pl.wikipedia.org/wiki/Kronos_Quartet",

					"https://pl.wikipedia.org/wiki/Kod_%C5%BAr%C3%B3d%C5%82owy",
					"https://pl.wikipedia.org/wiki/Program_komputerowy",
					"https://pl.wikipedia.org/wiki/J%C4%99zyk_programowania",
					"https://pl.wikipedia.org/wiki/Kompilator",
					"https://pl.wikipedia.org/wiki/Maszyna_wirtualna" };

			String content;

			List<LinkedHashMap<String, Integer>> allDictionary = new ArrayList<LinkedHashMap<String, Integer>>();

			for (int i = 0; i < allWebPages.length; i++) {
				Helper.getContentPage(allWebPages[i], NAME + i + ".");

				content = Helper.openDedicatedFile("txt", NAME + i + ".");

				allDictionary.add(naiveAlgorithm(content));
			}

			Set<String> allWords = getAllUniqueWords(allDictionary);

			List<String> words = new ArrayList<String>();

			for (String s : allWords) {
				words.add(s);
			}

			List<List<Double>> allVectors = new ArrayList<List<Double>>();
			List<Double> temp = new ArrayList<Double>();

			for (HashMap<String, Integer> dict : allDictionary) {
				temp = new ArrayList<Double>();
				for (String s : words) {
					if (dict.containsKey(s)) {
						temp.add((double) dict.get(s) / dict.size());
					} else {
						temp.add((double) 0);
					}
				}
				allVectors.add(temp);
			}

			printNTheBestTheLeast(calculateLikeHood(allVectors));

			printAllWebPage(allWebPages);

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public static void printAllWebPage(String[] allWebPages) {

		System.out.println("\n");

		for (int i = 0; i < allWebPages.length; i++) {

			System.out.println("Wektor " + i + ": " + allWebPages[i]);
		}
	}

	public static Map<Double, String> calculateLikeHood(
			List<List<Double>> allVectors) {

		double result;
		double numerator = 0; // licznik

		double tempFirstVector = 0;
		double tempSecondVector = 0;

		Map<Double, String> results = new TreeMap<Double, String>();

		List<Double> tempList = new ArrayList<Double>();

		for (int i = 0; i < 15; i++) {
			tempList.add((double) i + 1);
		}

		for (int i = 0; i < allVectors.size(); i++) {
			for (int j = i + 1; j < allVectors.size(); j++) {
				for (int k = 0; k < allVectors.get(i).size(); k++) {
					numerator += allVectors.get(i).get(k)
							* allVectors.get(j).get(k);
					tempFirstVector += Math.pow(allVectors.get(i).get(k), 2);
					tempSecondVector += Math.pow(allVectors.get(j).get(k), 2);
				}

				result = numerator
						/ (Math.sqrt(tempFirstVector) * Math
								.sqrt(tempSecondVector));

				results.put(result, "Wektor " + i + " : Wektor " + j);
			}
		}

		return results;

	}

	public static void printNTheBestTheLeast(Map<Double, String> results) {
		List<Double> keys = new ArrayList<Double>();
		List<String> values = new ArrayList<String>();

		for (double key : results.keySet()) {
			keys.add(key);
			values.add(results.get(key));
		}

		System.out.println("\n" + MAX + " najbardziej podobnych dokumentów\n");

		for (int i = keys.size() - 1; i > keys.size() - 1 - MIN; i--) {
			System.out.println(keys.get(i) + " = " + values.get(i));
		}

		System.out.println("\n" + MIN + " najmniej podobnych dokumentów\n");

		for (int i = 0; i < MAX; i++) {
			System.out.println(keys.get(i) + " = " + values.get(i));
		}

	}

	public static Set<String> getAllUniqueWords(
			List<LinkedHashMap<String, Integer>> allDictionary) {

		Set<String> allWords = new TreeSet<String>();

		for (HashMap m : allDictionary) {
			allWords.addAll(m.keySet());
		}

		return allWords;
	}

	public static LinkedHashMap<String, Integer> naiveAlgorithm(String content) {
		String allWords[] = splitToWords(content);

		sortWords(allWords);

		LinkedHashMap<String, Integer> words = createMapWordsWithCount(allWords);

		words = removeAllNumberUnnecessarySymbol(words);

		return words;
	}

	public static void selectTheMostOftenWords(
			LinkedHashMap<String, Integer> words) {
		int countWord = 0;

		String key;
		Integer value;

		LinkedHashMap<String, Integer> theMostOftenWords = new LinkedHashMap<String, Integer>();

		for (Entry<String, Integer> e : words.entrySet()) {
			key = e.getKey();
			value = e.getValue();

			if (countWord >= K) {
				break;
			}

			if (value >= THRESH) {
				countWord++;

				theMostOftenWords.put(key, value);
			}
		}

		// System.out.println(Helper.sortHashMapWithWords(theMostOftenWords));

	}

	public static LinkedHashMap<String, Integer> removeAllNumberUnnecessarySymbol(
			LinkedHashMap<String, Integer> allWords) {

		allWords.remove("");
		allWords.remove(" ");
		allWords.remove("nbsp");
		allWords.remove("\n");

		String pattern = "[0-9]";
		Pattern r = Pattern.compile(pattern);

		List<String> keyToRemove = new ArrayList<String>();

		for (String key : allWords.keySet()) {

			Matcher m = r.matcher(key);

			if (m.find()) {
				keyToRemove.add(key);
			}
		}

		for (String key : keyToRemove) {
			allWords.remove(key);
		}

		return allWords;
	}

	public static double calculateDuringTime(double startTime, double endTime) {
		return endTime - startTime;
	}

	public static LinkedHashMap<String, Integer> createMapWordsWithCount(
			String[] allWords) {

		LinkedHashMap<String, Integer> words = new LinkedHashMap<String, Integer>();

		for (String word : allWords) {
			if (words.containsKey(word)) {
				words.put(word, words.get(word) + 1);
			} else {
				words.put(word, 1);
			}
		}

		return words;
	}

	public static String[] splitToWords(String content) {
		return content.split(" ");
	}

	public static void sortWords(String[] allWords) {
		Arrays.sort(allWords);
	}

}
