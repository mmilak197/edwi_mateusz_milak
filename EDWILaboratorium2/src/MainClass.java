import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainClass {

	private static int THRESH = 4;
	private static int K = 10;

	public static void main(String[] args) {
		try {

			Helper.getContentPage();

			String content = Helper.openDedicatedFile("txt");

			double startTime = System.nanoTime();
			naiveAlgorithm(content);
			double endTime = System.nanoTime();

			double allDuringTime = calculateDuringTime(startTime, endTime);

			System.out.println("naiveAlgorithm : " + allDuringTime);

			String allWords[] = splitToWords(content);

			startTime = System.nanoTime();
			myAlgorithm(allWords);
			endTime = System.nanoTime();

			allDuringTime = calculateDuringTime(startTime, endTime);

			System.out.println("myAlgorithm : " + allDuringTime);

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public static void naiveAlgorithm(String content) {
		String allWords[] = splitToWords(content);

		sortWords(allWords);

		LinkedHashMap<String, Integer> words = createMapWordsWithCount(allWords);

		words = removeAllNumberUnnecessarySymbol(words);

		selectTheMostOftenWords((LinkedHashMap<String, Integer>) Helper
				.sortHashMapWithWords(words));
	}

	public static void myAlgorithm(String[] allWords) {

		sortWords(allWords);

		MyObject[] hashTable = new MyObject[32];

		for (int i = 0; i < hashTable.length; i++) {
			hashTable[i] = new MyObject();
		}

		for (int i = 0; i < allWords.length; i++) {
			if (!allWords[i].equals("")) {
				String newElementVal = allWords[i];

				int arrayIndex = newElementVal.charAt(0) % 32 - 1;

				if (hashTable[arrayIndex].getMatchWord().isEmpty()) {

					hashTable[arrayIndex].getMatchWord().put(newElementVal, 1);
				} else {
					if (hashTable[arrayIndex].getMatchWord().containsKey(
							newElementVal)) {
						hashTable[arrayIndex].getMatchWord().put(
								newElementVal,
								hashTable[arrayIndex].getMatchWord().get(
										newElementVal) + 1);
					} else {
						hashTable[arrayIndex].getMatchWord().put(newElementVal,
								1);
					}
				}
			}

		}

		LinkedHashMap<String, Integer> oftenWords = new LinkedHashMap<String, Integer>();

		for (int i = 0; i < hashTable.length; i++) {

			hashTable[i].setMatchWord((LinkedHashMap<String, Integer>) Helper
					.sortHashMapWithWords(hashTable[i].getMatchWord()));

			for (Entry<String, Integer> e : hashTable[i].getMatchWord()
					.entrySet()) {

				if (e.getValue() >= THRESH) {
					oftenWords.put(e.getKey(), e.getValue());
				}

				else {
					break;
				}

			}

		}

		oftenWords = (LinkedHashMap<String, Integer>) Helper
				.sortHashMapWithWords(oftenWords);

		oftenWords = removeAllNumberUnnecessarySymbol(oftenWords);

		selectTheMostOftenWords(oftenWords);

	}

	public static void selectTheMostOftenWords(
			LinkedHashMap<String, Integer> words) {
		int countWord = 0;

		String key;
		Integer value;

		LinkedHashMap<String, Integer> theMostOftenWords = new LinkedHashMap<String, Integer>();

		for (Entry<String, Integer> e : words.entrySet()) {
			key = e.getKey();
			value = e.getValue();

			if (countWord >= K) {
				break;
			}

			if (value >= THRESH) {
				countWord++;

				theMostOftenWords.put(key, value);
			}
		}

		System.out.println(Helper.sortHashMapWithWords(theMostOftenWords));

	}

	public static LinkedHashMap<String, Integer> removeAllNumberUnnecessarySymbol(
			LinkedHashMap<String, Integer> allWords) {

		allWords.remove("");
		allWords.remove(" ");
		allWords.remove("\n");
		allWords.remove("nbsp");

		String pattern = "[0-9]";
		Pattern r = Pattern.compile(pattern);

		List<String> keyToRemove = new ArrayList<String>();

		for (String key : allWords.keySet()) {

			Matcher m = r.matcher(key);

			if (m.find()) {
				keyToRemove.add(key);
			}
		}

		for (String key : keyToRemove) {
			allWords.remove(key);
		}

		return allWords;
	}

	public static double calculateDuringTime(double startTime, double endTime) {
		return (endTime - startTime) / 1000000;
	}

	public static LinkedHashMap<String, Integer> createMapWordsWithCount(
			String[] allWords) {

		LinkedHashMap<String, Integer> words = new LinkedHashMap<String, Integer>();

		for (String word : allWords) {
			if (words.containsKey(word)) {
				words.put(word, words.get(word) + 1);
			} else {
				words.put(word, 1);
			}
		}

		return words;
	}

	public static String[] splitToWords(String content) {
		return content.split(" ");
	}

	public static void sortWords(String[] allWords) {
		Arrays.sort(allWords);
	}

}
