import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class Helper {

	public static String NAME_FILE_WITH_DOT = "index.";
	public static int LENGTH_WORD_CHARSET_WITH_CHAR_EGUALS = 8;

	public static String LINK_TO_WEBPAGE = "http://www.wp.pl/";

	// http://pduch.kis.p.lodz.pl/, http://www.wp.pl/
	// http://radamus.kis.p.lodz.pl/
	// http://www.onet.pl/

	// https://pl.wikipedia.org/wiki/S%C5%82o%C5%84_afryka%C5%84ski

	public static String getCharsetWebPage(String contentWebPage)
			throws Exception {

		int fromIndex = contentWebPage.indexOf("charset=");

		int toIndex = contentWebPage.indexOf("\"", fromIndex
				+ LENGTH_WORD_CHARSET_WITH_CHAR_EGUALS + 1);

		return contentWebPage.substring(
				fromIndex + LENGTH_WORD_CHARSET_WITH_CHAR_EGUALS, toIndex)
				.replaceAll("[^a-zA-Z0-9-]", "");

	}

	public static String getWebPage(String linkToWebPage)
			throws MalformedURLException, IOException {

		URL url = new URL(linkToWebPage);
		URLConnection con = url.openConnection();
		InputStream in = con.getInputStream();
		String encoding = con.getContentEncoding();
		encoding = encoding == null ? "UTF-8" : encoding;
		String page = IOUtils.toString(in, encoding);
		
		return page;
	}

	public static void saveContentToDedicatedFile(String contentWebPage,
			String extension) {

		File file = new File(NAME_FILE_WITH_DOT + extension);

		try {
			if (!file.exists()) {
				file.createNewFile();
			}

			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(contentWebPage);
			bw.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static String openDedicatedFile(String extension) {

		String sCurrentLine;
		StringBuffer allContentFile = new StringBuffer();

		try (BufferedReader br = new BufferedReader(new FileReader(
				NAME_FILE_WITH_DOT + extension))) {

			while ((sCurrentLine = br.readLine()) != null) {

				allContentFile.append(sCurrentLine + "\n");
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

		return allContentFile.toString();
	}

	public static String getBodyContent(String contentWebPage) {

		Document doc = Jsoup.parse(contentWebPage);
		String body = doc.body().text();

		return body;
	}

	public static String replaceHTMLTag(String bodyContentWebPage) {

		String temp = bodyContentWebPage.replaceAll("\\<.*?>", "");

		temp = temp.replaceAll("<!--", "").replaceAll("-->", "");

		return temp;
	}

	public static String replaceAllWhiteChars(String content) {
		content = content.replaceAll("[^a-zA-Z0-9���󜳿��]", " ");
		content = content.replaceAll("[  ]+", " ");

		return content;
	}

	public static String replaceAllUpperCaseToLowerCase(String content) {
		return content.toLowerCase();
	}

	public static Map<String, Integer> sortHashMapWithWords(
			Map<String, Integer> unsortMap) {

		List<Map.Entry<String, Integer>> list = new LinkedList<Map.Entry<String, Integer>>(
				unsortMap.entrySet());

		Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
			public int compare(Map.Entry<String, Integer> o1,
					Map.Entry<String, Integer> o2) {
				return (o2.getValue()).compareTo(o1.getValue());
			}
		});

		Map<String, Integer> sortedMap = new LinkedHashMap<String, Integer>();
		for (Iterator<Map.Entry<String, Integer>> it = list.iterator(); it
				.hasNext();) {
			Map.Entry<String, Integer> entry = it.next();
			sortedMap.put(entry.getKey(), entry.getValue());
		}
		return sortedMap;
	}

	public static void getContentPage() throws MalformedURLException,
			IOException {
		String contentWebPage = Helper.getWebPage(Helper.LINK_TO_WEBPAGE);

		Helper.saveContentToDedicatedFile(contentWebPage, "html");

		contentWebPage = Helper.openDedicatedFile("html");

		String body = Helper.getBodyContent(contentWebPage);

		body = Helper.replaceHTMLTag(body);

		body = Helper.replaceAllUpperCaseToLowerCase(body);

		body = Helper.replaceAllWhiteChars(body);

		System.out.println(body);

		Helper.saveContentToDedicatedFile(body, "txt");
	}

	public static void getBodySection() {
		try {
			URL url = new URL("http://www.onet.pl/");
			URLConnection con = url.openConnection();
			InputStream in = con.getInputStream();
			String encoding = con.getContentEncoding();
			encoding = encoding == null ? "UTF-8" : encoding;
			String body = IOUtils.toString(in, encoding);

			Document doc = Jsoup.parse(body);
			String text = doc.body().text();

		} catch (Exception e) {
			System.err.println("B�ad");
		}
	}

}
