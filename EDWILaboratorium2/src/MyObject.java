import java.util.LinkedHashMap;

public class MyObject {

	private LinkedHashMap<String, Integer> matchWord;

	public MyObject() {
		matchWord = new LinkedHashMap<String, Integer>();
	}

	public LinkedHashMap<String, Integer> getMatchWord() {
		return matchWord;
	}

	public void setMatchWord(LinkedHashMap<String, Integer> matchWord) {
		this.matchWord = matchWord;
	}

}
