import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import org.apache.commons.io.IOUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class MainClass {

	private static String NAME_FILE_WITH_DOT = "index.";

	private static String LINK_TO_WEBPAGE = "http://www.onet.pl/";

	// http://pduch.kis.p.lodz.pl/, http://www.zakrzewska-bielawska.pl/,
	// http://radamus.kis.p.lodz.pl/, http://www.wp.pl

	public static void main(String[] args) {
		try {

			String contentWebPage = getWebPage(LINK_TO_WEBPAGE);

			saveContentToDedicatedFile(contentWebPage, "html");

			contentWebPage = openDedicatedFile("html");

			String body = getBodyContent(contentWebPage);

			body = replaceHTMLTag(body);

			body = replaceAllUpperCaseToLowerCase(body);

			body = replaceAllWhiteChars(body);

			System.out.println(body);

			saveContentToDedicatedFile(body, "txt");

		} catch (MalformedURLException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public static String getWebPage(String linkToWebPage)
			throws MalformedURLException, IOException {

		URL url = new URL(linkToWebPage);
		URLConnection con = url.openConnection();
		InputStream in = con.getInputStream();
		String encoding = con.getContentEncoding();
		encoding = encoding == null ? "UTF-8" : encoding;
		String page = IOUtils.toString(in, encoding);

		return page;
	}

	public static void saveContentToDedicatedFile(String contentWebPage,
			String extension) {

		File file = new File(NAME_FILE_WITH_DOT + extension);

		try {
			if (!file.exists()) {
				file.createNewFile();
			}

			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(contentWebPage);
			bw.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static String openDedicatedFile(String extension) {

		String sCurrentLine;
		StringBuffer allContentFile = new StringBuffer();

		try (BufferedReader br = new BufferedReader(new FileReader(
				NAME_FILE_WITH_DOT + extension))) {

			while ((sCurrentLine = br.readLine()) != null) {

				allContentFile.append(sCurrentLine + "\n");
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

		return allContentFile.toString();
	}

	public static String getBodyContent(String contentWebPage) {

		Document doc = Jsoup.parse(contentWebPage);
		String body = doc.body().text();

		return body;
	}

	public static String replaceHTMLTag(String bodyContentWebPage) {

		String temp = bodyContentWebPage.replaceAll("\\<.*?>", "");

		temp = temp.replaceAll("<!--", "").replaceAll("-->", "");

		return temp;
	}

	public static String replaceAllWhiteChars(String content) {
		content = content.replaceAll("&nbsp", "");
		content = content.replaceAll("[^a-zA-Z0-9���󜳿��]", " ");
		content = content.replaceAll("[  ]+", " ");

		return content;
	}

	public static String replaceAllUpperCaseToLowerCase(String content) {
		return content.toLowerCase();
	}

}
