import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class FilesContentReader {
	private static final boolean RECURSIVE = false;

	public static List<String> allTitleBooks = new ArrayList<String>();
	public static Set<String> uniqueTitles = new HashSet<String>();
	public static List<String> contents = new ArrayList<String>();
	
	public static List<Book> allBooks = new ArrayList<Book>();

	public static void printContentOfFilesInDirectory(File directory)
			throws IOException {

		File[] files = directory.listFiles();

		for (File file : files) {

			if (file.isFile()) {

				printFile(file);

			} else if (file.isDirectory() && RECURSIVE) {

				printContentOfFilesInDirectory(file);

			}

		}

	}

	private static void printFile(File file) throws IOException {

		BufferedReader reader = new BufferedReader(new FileReader(file));

		try {

			addTitleAndContent(reader);
			// System.out.println("Odczytano !");

		} finally {

			reader.close();

		}

	}

	
	
	private static void addTitleAndContent(BufferedReader reader) throws IOException
	{
		
		String line = null;
		
		boolean find = false;
		StringBuffer allContent = new StringBuffer();
		
		Book book = new Book();
		
		while((line = reader.readLine()) != null )
		{
			
			if(find)
			{
				allContent.append(line);
			}
			
			else if(line.contains("Title: "))
			{
				//uniqueTitles.add(catTitleFromLine(line));
				book.setTitle(catTitleFromLine(line));
			}
			
			else if(line.contains("*** START OF THIS PROJECT GUTENBERG EBOOK"))
			{
				find = true;
			}
			
			
		}
		
		contents.add(allContent.toString());
		book.setContent(allContent.toString());
		
		
		allBooks.add(book);
		
		MainClass.addDoc(MainClass.w, book.getTitle(), book.getContent());
	}

	public static String catTitleFromLine(String line) {
		return line.replace("Title: ", "");
	}
	
	

}
