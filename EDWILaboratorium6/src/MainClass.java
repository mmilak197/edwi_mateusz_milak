import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

public class MainClass {

	public static final Version version = Version.LUCENE_36;
	
	public static IndexWriter w;

	public static String indexDirectory = "E:\\MyWorkspace\\PracaMagisterskaJavaLuty\\EDWILab6IndexDirectory";

	public static void main(String[] args) throws IOException, ParseException {

		String dirPathname = "E:\\MyWorkspace\\PracaMagisterskaJavaLuty\\guttenberg_part";

		File directory = new File(dirPathname);
		
		StandardAnalyzer analyzer = new StandardAnalyzer(version);

		Directory index = FSDirectory.open(new File(indexDirectory));

		IndexWriterConfig config = new IndexWriterConfig(version, analyzer);

		 w = new IndexWriter(index, config);

		if (!directory.isDirectory()) {

			System.out.println(dirPathname + " is not directory");

		}
		
		//FilesContentReader.printContentOfFilesInDirectory(directory);

		

		/*for (Book book : FilesContentReader.allBooks) {
			addDoc(w, book.getTitle(), book.getContent());
		}*/
		
		
		w.close();

		String querystr = args.length > 0 ? args[0] : "Wendy";

		Query q = new QueryParser(version, "content", analyzer).parse(querystr);

		int hitsPerPage = 100;
		IndexReader reader = DirectoryReader.open(index);
		IndexSearcher searcher = new IndexSearcher(reader);
		TopDocs docs = searcher.search(q, hitsPerPage);
		ScoreDoc[] hits = docs.scoreDocs;

		// 4. display results
		System.out.println("Found " + hits.length + " hits.");
		for (int i = 0; i < hits.length; ++i) {
			int docId = hits[i].doc;
			Document d = searcher.doc(docId);
			System.out.println((i + 1) + ". " + "\t" + d.get("title"));
			
		}

		reader.close();
		
	}

	public static void addDoc(IndexWriter w, String title, String content)
			throws IOException {
		Document doc = new Document();
		doc.add(new TextField("title", title, Field.Store.YES));

		doc.add(new TextField("content", content, Field.Store.YES));

		w.addDocument(doc);
	}
}
