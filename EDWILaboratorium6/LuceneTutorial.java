import java.io.File;
import java.io.IOException;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryParser.ParseException;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.LockObtainFailedException;
import org.apache.lucene.store.RAMDirectory;
import org.apache.lucene.util.Version;


public class LuceneTutorial
{
	public static final Version version = Version.LUCENE_36;
	public static final String indexDirectory = "z:\\Projects\\UserFiles\\LuceneTut";
	
	public static void main(String[] args) throws CorruptIndexException, LockObtainFailedException, IOException, ParseException
	{
		//create index in memory:
		//Directory index = new RAMDirectory();
		
		// Create index on disk
		Directory index = FSDirectory.open(new File(indexDirectory));
		
		String query = "Molly";

		LuceneTutorial lucene = new LuceneTutorial();
		//lucene.buildIndex(index); // building should be performed only once when storing index on disk. 
		lucene.useIndex(index, query);
	}
	
	private void useIndex(Directory index, String query) throws ParseException, CorruptIndexException, IOException
	{
		StandardAnalyzer analyzer = new StandardAnalyzer(version);
		
		// go to: http://www.lucenetutorial.com/lucene-query-syntax.html
		// look at: http://www.lucenetutorial.com/lucene-query-builder.html
		Query q = new QueryParser(version, "title", analyzer).parse(query);
		
		// Perform search
		int hitsPerPage = 5;
		
		IndexReader reader = IndexReader.open(index);
		IndexSearcher searcher = new IndexSearcher(reader);
		
		TopScoreDocCollector collector = TopScoreDocCollector.create(hitsPerPage, true);
		
		searcher.search(q, collector);
		
		ScoreDoc[] hits = collector.topDocs().scoreDocs;
		   
		// display results
		System.out.println("Found " + hits.length + " hits.");
		for(int i=0;i<hits.length;++i)
		{
			int docId = hits[i].doc;
			Document d = searcher.doc(docId);
			System.out.println((i + 1) + ". " + d.get("title"));
		}
		
		// searcher can only be closed when there
		// is no need to access the documents any more.
		searcher.close();
	}
	
	private void buildIndex(Directory index) throws IOException
	{
		StandardAnalyzer analyzer = new StandardAnalyzer(version);
		
		IndexWriterConfig config = new IndexWriterConfig(version, analyzer);
	
		// Add some titles
		IndexWriter indexWriter = new IndexWriter(index, config);

		indexWriter.addDocument(makeDocTitle("Hello World"));
		indexWriter.addDocument(makeDocTitle("Hello Molly"));
		indexWriter.addDocument(makeDocTitle("Hello Molly 2"));
		indexWriter.addDocument(makeDocTitle("Hello Molly 3"));
		indexWriter.addDocument(makeDocTitle("Hello Molly 4"));
		indexWriter.addDocument(makeDocTitle("Hello Molly 5"));
		indexWriter.addDocument(makeDocTitle("Hello Molly 6"));
		indexWriter.addDocument(makeDocTitle("Hello Molly 7"));
		indexWriter.addDocument(makeDocTitle("Hello Molly 8"));
		indexWriter.addDocument(makeDocTitle("Hello Molly 9"));
		indexWriter.addDocument(makeDocTitle("Hello Molly 10"));
		indexWriter.addDocument(makeDocTitle("Hello Molly 11"));
		indexWriter.addDocument(makeDocTitle("Hello Molly 12"));
		indexWriter.addDocument(makeDocTitle("Hello Molly 13"));
		indexWriter.addDocument(makeDocTitle("Molly like dogs"));
		indexWriter.addDocument(makeDocTitle("Dogs are sweet"));
		
		indexWriter.close();
	}

	private static Document makeDocTitle(String title)
	{
		Document doc = new Document();
		doc.add(new Field("title", title, Field.Store.YES, Field.Index.ANALYZED));
		return doc;
	}
}
