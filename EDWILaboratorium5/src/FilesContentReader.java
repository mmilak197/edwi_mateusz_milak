import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;

public class FilesContentReader {

	private static final boolean RECURSIVE = false;

	public static List<String> allTitleBooks = new ArrayList<String>();
	public static Set<String> uniqueTitles = new HashSet<String>();

	public static void printContentOfFilesInDirectory(File directory)
			throws IOException {

		File[] files = directory.listFiles();

		for (File file : files) {

			if (file.isFile()) {

				printFile(file);

			} else if (file.isDirectory() && RECURSIVE) {

				printContentOfFilesInDirectory(file);

			}

		}

	}

	private static void printFile(File file) throws IOException {

		BufferedReader reader = new BufferedReader(new FileReader(file));

		try {

			printReaderContent(reader);

		} finally {

			reader.close();

		}

	}

	private static void printReaderContent(BufferedReader reader)
			throws IOException {

		String line = null;

		while ((line = reader.readLine()) != null) {

			if (line.contains("Title: ")) {

				MainClass.addDoc(MainClass.w, catTitleFromLine(line));

				break;
			}

		}

	}

	public static String catTitleFromLine(String line) {
		return line.replace("Title: ", "");
	}

}