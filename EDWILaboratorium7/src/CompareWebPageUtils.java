import indexContent.WebPage;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.IOUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class CompareWebPageUtils {

	private static int THRESH = 4;
	private static int K = 10;

	private static int MAX = 10;
	private static int MIN = 10;

	private static String NAME = "SAMPLE";

	public static void startCompare(List<WebPage> allWebPages,
			List<String> selectedLinks) {
		try {

			String content;

			List<LinkedHashMap<String, Integer>> allDictionary = new ArrayList<LinkedHashMap<String, Integer>>();

			for (int i = 0; i < allWebPages.size(); i++) {

				content = allWebPages.get(i).getContent();

				allDictionary.add(naiveAlgorithm(content));
			}
			
			List<WebPage> selectedLinksWebPage = copySelectedItem(allWebPages, selectedLinks);
			
			List<LinkedHashMap<String, Integer>> selectedDictionary = new ArrayList<LinkedHashMap<String, Integer>>();
			
			for (int i = 0; i < selectedLinksWebPage.size(); i++) {

				content = selectedLinksWebPage.get(i).getContent();

				selectedDictionary.add(naiveAlgorithm(content));
			}
			
			// allDictionary list slow wraz z ich wystapieniami dla kazdego link

			Set<String> allWords = getAllUniqueWords(allDictionary);

			List<String> words = new ArrayList<String>();

			// Lista wszystkich slow
			for (String s : allWords) {
				words.add(s);
			}

			List<List<Double>> allVectors = new ArrayList<List<Double>>();
			List<Double> temp = new ArrayList<Double>();

			for (HashMap<String, Integer> dict : allDictionary) {
				temp = new ArrayList<Double>();
				for (String s : words) {
					if (dict.containsKey(s)) {
						temp.add((double) dict.get(s) / dict.size());
					} else {
						temp.add((double) 0);
					}
				}
				allVectors.add(temp);
			}
			
			List<List<Double>> allSelectedVectors = new ArrayList<List<Double>>();
			temp = new ArrayList<Double>();
			
			for (HashMap<String, Integer> dict : selectedDictionary) {
				temp = new ArrayList<Double>();
				for (String s : words) {
					if (dict.containsKey(s)) {
						temp.add((double) dict.get(s) / dict.size());
					} else {
						temp.add((double) 0);
					}
				}
				allSelectedVectors.add(temp);
			}

			// allVectors - tutaj jest lista z [1/4, 0, 0, 0, 1/4, 0, 1/4, 0, 0,
			// 1/4, 0] dla kazdego linku

			printNTheBestTheLeast(calculateLikeHoodNew(allVectors, allSelectedVectors));

			// drukowanie wszystkich stron
			// printAllWebPage(allWebPages);

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public static void printAllWebPage(List<WebPage> allWebPages) {

		System.out.println("\n");

		for (int i = 0; i < allWebPages.size(); i++) {

			System.out.println("Wektor " + i + ": "
					+ allWebPages.get(i).getContent());
		}
	}

	public static Map<Double, String> calculateLikeHoodNew(
			List<List<Double>> allVectors, List<List<Double>> selectedVectors) {

		double numerator = 0;

		double result;
		double tempFirstVector = 0;
		double tempSecondVector = 0;

		Map<Double, String> results = new TreeMap<Double, String>();

		List<Double> tempList = new ArrayList<Double>();

		for (int i = 0; i < allVectors.size(); i++) {
			for (int j = 0; j < allVectors.get(i).size(); j++) {
				for (int k = 0; k < selectedVectors.size(); k++) {
					for (int m = 0; m < selectedVectors.get(k).size(); m++) {
						numerator += allVectors.get(i).get(j)
								* selectedVectors.get(k).get(m);

						tempFirstVector += Math
								.pow(allVectors.get(i).get(j), 2);
						tempSecondVector += Math.pow(allVectors.get(k).get(m),
								2);

					}

					result = numerator
							/ (Math.sqrt(tempFirstVector) * Math
									.sqrt(tempSecondVector));

					results.put(result, "Wektor " + i);
				}
			}
		}

		return results;

	}

	// ///////////////////// tutaj zajrzec !!!!!!!!
	// do zmiany metoda calculateLikeHood

	/*
	 * public static Map<Double, String> calculateLikeHood( List<List<Double>>
	 * allVectors) {
	 * 
	 * double result; double numerator = 0; // licznik
	 * 
	 * double tempFirstVector = 0; double tempSecondVector = 0;
	 * 
	 * Map<Double, String> results = new TreeMap<Double, String>();
	 * 
	 * List<Double> tempList = new ArrayList<Double>();
	 * 
	 * for (int i = 0; i < 15; i++) { tempList.add((double) i + 1); }
	 * 
	 * for (int i = 0; i < allVectors.size(); i++) { for (int j = i + 1; j <
	 * allVectors.size(); j++) { for (int k = 0; k < allVectors.get(i).size();
	 * k++) { numerator += allVectors.get(i).get(k) allVectors.get(j).get(k);
	 * tempFirstVector += Math.pow(allVectors.get(i).get(k), 2);
	 * tempSecondVector += Math.pow(allVectors.get(j).get(k), 2); }
	 * 
	 * result = numerator / (Math.sqrt(tempFirstVector) * Math
	 * .sqrt(tempSecondVector));
	 * 
	 * results.put(result, "Wektor " + i + " : Wektor " + j); } }
	 * 
	 * return results;
	 * 
	 * }
	 */

	public static void printNTheBestTheLeast(Map<Double, String> results) {
		List<Double> keys = new ArrayList<Double>();
		List<String> values = new ArrayList<String>();

		for (double key : results.keySet()) {
			keys.add(key);
			values.add(results.get(key));
		}

		System.out.println("\n" + MAX + " najbardziej podobnych dokumentów\n");

		for (int i = keys.size() - 1; i > keys.size() - 1 - MIN; i--) {
			System.out.println(keys.get(i) + " = " + values.get(i));
		}

		System.out.println("\n" + MIN + " najmniej podobnych dokumentów\n");

		for (int i = 0; i < MAX; i++) {
			System.out.println(keys.get(i) + " = " + values.get(i));
		}

	}

	public static Set<String> getAllUniqueWords(
			List<LinkedHashMap<String, Integer>> allDictionary) {

		Set<String> allWords = new TreeSet<String>();

		for (HashMap m : allDictionary) {
			allWords.addAll(m.keySet());
		}

		return allWords;
	}

	public static LinkedHashMap<String, Integer> naiveAlgorithm(String content) {
		String allWords[] = splitToWords(content);

		sortWords(allWords);

		LinkedHashMap<String, Integer> words = createMapWordsWithCount(allWords);

		words = removeAllNumberUnnecessarySymbol(words);

		return words;
	}

	public static void selectTheMostOftenWords(
			LinkedHashMap<String, Integer> words) {
		int countWord = 0;

		String key;
		Integer value;

		LinkedHashMap<String, Integer> theMostOftenWords = new LinkedHashMap<String, Integer>();

		for (Entry<String, Integer> e : words.entrySet()) {
			key = e.getKey();
			value = e.getValue();

			if (countWord >= K) {
				break;
			}

			if (value >= THRESH) {
				countWord++;

				theMostOftenWords.put(key, value);
			}
		}

		// System.out.println(Helper.sortHashMapWithWords(theMostOftenWords));

	}

	public static LinkedHashMap<String, Integer> removeAllNumberUnnecessarySymbol(
			LinkedHashMap<String, Integer> allWords) {

		allWords.remove("");
		allWords.remove(" ");
		allWords.remove("nbsp");
		allWords.remove("\n");

		String pattern = "[0-9]";
		Pattern r = Pattern.compile(pattern);

		List<String> keyToRemove = new ArrayList<String>();

		for (String key : allWords.keySet()) {

			Matcher m = r.matcher(key);

			if (m.find()) {
				keyToRemove.add(key);
			}
		}

		for (String key : keyToRemove) {
			allWords.remove(key);
		}

		return allWords;
	}

	public static double calculateDuringTime(double startTime, double endTime) {
		return endTime - startTime;
	}

	public static LinkedHashMap<String, Integer> createMapWordsWithCount(
			String[] allWords) {

		LinkedHashMap<String, Integer> words = new LinkedHashMap<String, Integer>();

		for (String word : allWords) {
			if (words.containsKey(word)) {
				words.put(word, words.get(word) + 1);
			} else {
				words.put(word, 1);
			}
		}

		return words;
	}
	
	public static List<WebPage> copySelectedItem(List<WebPage> allWebPages, List<String> selectedLinks){
		
		List<WebPage> finalLinks = new ArrayList<WebPage>();
		
		//for(String selectedLink : selectedLinks)
		for(int j = 0; j < 1; j++)
		{
			for(int i = 0; i < allWebPages.size(); i++)
			{
				if(selectedLinks.get(j).equals(allWebPages.get(i).getLink()))
				{
					finalLinks.add(allWebPages.get(i));
				}
			}
		}
		
		return finalLinks;
	}
			

	public static String[] splitToWords(String content) {
		return content.split(" ");
	}

	public static void sortWords(String[] allWords) {
		Arrays.sort(allWords);
	}

}
