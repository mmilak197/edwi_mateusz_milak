import indexContent.WebPage;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import org.apache.lucene.queryparser.classic.ParseException;

public class IndexContentUtils {

	private static final boolean RECURSIVE = false;

	public static List<String> allLinkWebPages = new ArrayList<String>();
	public static Set<String> uniqueLinks = new HashSet<String>();
	public static List<String> contents = new ArrayList<String>();

	public static List<WebPage> allWebPages = new ArrayList<WebPage>();

	public static final Version version = Version.LUCENE_36;

	public static IndexWriter w;

	public static String indexDirectory = "E:\\MyWorkspace\\PracaMagisterskaJavaLuty\\EDWILab7IndexDirectory";

	public static List<String> startIndexed(List<WebPage> contentAllWebPages, String question)
			throws IOException, ParseException {

		// String dirPathname =
		// "E:\\MyWorkspace\\PracaMagisterskaJavaLuty\\guttenberg_part";

		// File directory = new File(dirPathname);

		StandardAnalyzer analyzer = new StandardAnalyzer(version);

		Directory index = FSDirectory.open(new File(indexDirectory));

		IndexWriterConfig config = new IndexWriterConfig(version, analyzer);

		w = new IndexWriter(index, config);

	/*	for (WebPage webPage : contentAllWebPages) {

			if (webPage != null) {
				addLinkAndContent(webPage.getLink(), webPage.getContent());
			}

		}*/


		w.close();

		//Szukanie po zawartosci strony
		//String querystr = "samoch�d";
		
		String querystr = question;
		
		//String querystr = "programista";

		Query q = new QueryParser(version, "content", analyzer).parse(querystr);
		
		//Szukanie po adresie strony www
		//String querystr = "vod.pl//programy-onetu";
		
		//Query q = new QueryParser(version, "link", analyzer).parse(querystr);

		int hitsPerPage = 5;
		IndexReader reader = DirectoryReader.open(index);
		IndexSearcher searcher = new IndexSearcher(reader);
		TopDocs docs = searcher.search(q, hitsPerPage);
		ScoreDoc[] hits = docs.scoreDocs;
		
		List<String> selectedLinks = new ArrayList<String>();

		// 4. display results
		//System.out.println("Found " + hits.length + " hits.");
		for (int i = 0; i < hits.length; ++i) {
			int docId = hits[i].doc;
			Document d = searcher.doc(docId);
			
			selectedLinks.add(d.get("link"));
			
			System.out.println((i + 1) + ". " + "\t" + d.get("link"));

		}

		reader.close();
		
		return selectedLinks;

	}

	public static void addDoc(IndexWriter w, String link, String content)
			throws IOException {
		Document doc = new Document();
		doc.add(new TextField("link", link, Field.Store.YES));

		doc.add(new TextField("content", content, Field.Store.YES));

		w.addDocument(doc);
	}

	private static void addLinkAndContent(String link, String contentPage)
			throws IOException {

		boolean find = false;
		WebPage webPage = new WebPage();

		contents.add(contentPage);
		webPage.setLink(link);
		webPage.setContent(contentPage);

		allWebPages.add(webPage);

		addDoc(w, webPage.getLink(), webPage.getContent());
	}

}
