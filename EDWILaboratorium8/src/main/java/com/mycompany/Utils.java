package com.mycompany;

import org.json.simple.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.simple.JSONArray;
import org.json.simple.parser.ParseException;
import org.json.simple.parser.JSONParser;

public class Utils {

	public static String procceddText(String result) {

		result = result.replaceAll("\"label\"", "label");
		result = result.replaceAll("\"pos\"", "pos");
		result = result.replaceAll("\"neg\"", "neg");
		result = result.replaceAll("\"neutral\"", "neutral");

		return result;
	}

	public static String returnLabel(String result) {
		String temp[] = result.split("label:");
		
		String label = temp[1].replace("}", "");
		
		double value = getValueLabel(label, result);
		
		return label + " - " + value;
	}
	
	public static Double getValueLabel(String label, String json)
	{
		
		double value;
		
		String temp[] = json.split(label.replaceAll(" ", "") + ": ");
		
		String values[] = temp[1].split(",");
		
		value = new Double(values[0].replaceAll("}", ""));
		
		return value;
	}
	
	public static Map<String, Double> sortByComparator(Map<String, Double> unsortMap) {

		// Convert Map to List
		List<Map.Entry<String, Double>> list = 
			new LinkedList<Map.Entry<String, Double>>(unsortMap.entrySet());

		// Sort list with comparator, to compare the Map values
		Collections.sort(list, new Comparator<Map.Entry<String, Double>>() {
			public int compare(Map.Entry<String, Double> o1,
                                           Map.Entry<String, Double> o2) {
				return (o1.getValue()).compareTo(o2.getValue());
			}
		});

		// Convert sorted map back to a Map
		Map<String, Double> sortedMap = new LinkedHashMap<String, Double>();
		for (Iterator<Map.Entry<String, Double>> it = list.iterator(); it.hasNext();) {
			Map.Entry<String, Double> entry = it.next();
			sortedMap.put(entry.getKey(), entry.getValue());
		}
		return sortedMap;
	}
	
	public static List<String> removeAllDigits(List<String> listAllWords)
	{
		List<String> temp = new ArrayList<String>();
		
		String tempWord;
		
		for(String word : listAllWords)
		{
			
			tempWord = word.replaceAll("[0-9]", "");
			
			if(!tempWord.equals(""))
			{
				temp.add(tempWord);
			}
			
		}
		
		return temp;
	}
	
	public static void printListWord(List<String> keys, List<Double> values)
	{
		int contion = 0;
		
		if(keys.size() > MainClass.K)
		{
			contion = keys.size() - MainClass.K;
		}
		
		for(int i = keys.size() - 1; i > contion; i--)
		{
			System.out.println(keys.get(i) + " - " + values.get(i));
		}
		
	}
	
	public static List<String> getAllWords(String allWords[])
	{
		Set<String> tempAllWords = new HashSet<String>(Arrays.asList(allWords));

		List<String> listAllWords = new ArrayList<String>();

		listAllWords.addAll(tempAllWords);

		listAllWords = Utils.removeAllDigits(listAllWords);
		
		return listAllWords;
	}


}
