package com.mycompany;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class GetWebPageUtils {

	private static String NAME_FILE_WITH_DOT = "index.";
	public static String FIRST_FILE_NAME = "connectTheSameServer";
	public static String SECOND_FILE_NAME = "connectToOtherServer";

	public static void saveToFile(String nameFile, List<String> allLinks) {
		File file = new File(nameFile + ".txt");

		StringBuffer contentToSave = new StringBuffer();

		for (String s : allLinks) {
			contentToSave.append(s + "\n");
		}

		try {
			if (!file.exists()) {
				file.createNewFile();
			}

			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(contentToSave.toString());
			bw.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static String processLink(String convertLink) {
		convertLink = convertLink.replace("http://", "")
				.replace("https://", "");

		if (convertLink.contains("/")) {
			int index = convertLink.indexOf("/");

			convertLink = convertLink.substring(0, index + 1);
		}

		if (convertLink.contains("?")) {
			int index = convertLink.indexOf("?");

			convertLink = convertLink.substring(0, index);
		}

		return convertLink = convertLink.replaceAll("/", "");
	}

	public static String getWebPage(String linkToWebPage)
			throws MalformedURLException, IOException {

		URL url = new URL(linkToWebPage);
		URLConnection con = url.openConnection();
		InputStream in = con.getInputStream();
		String encoding = con.getContentEncoding();
		encoding = encoding == null ? "UTF-8" : encoding;
		String page = IOUtils.toString(in, encoding);

		return page;
	}

	public static void saveContentToDedicatedFile(String contentWebPage,
			String extension) {

		File file = new File(NAME_FILE_WITH_DOT + extension);

		try {
			if (!file.exists()) {
				file.createNewFile();
			}

			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(contentWebPage);
			bw.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static String openDedicatedFile(String extension) {

		String sCurrentLine;
		StringBuffer allContentFile = new StringBuffer();

		try (BufferedReader br = new BufferedReader(new FileReader(
				NAME_FILE_WITH_DOT + extension))) {

			while ((sCurrentLine = br.readLine()) != null) {

				allContentFile.append(sCurrentLine + "\n");
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

		return allContentFile.toString();
	}

	public static String getBodyContent(String contentWebPage) {

		Document doc = Jsoup.parse(contentWebPage);
		String body = doc.body().text();

		return body;
	}

	public static String replaceHTMLTag(String bodyContentWebPage) {

		String temp = bodyContentWebPage.replaceAll("\\<.*?>", "");

		temp = temp.replaceAll("<!--", "").replaceAll("-->", "");

		return temp;
	}

	public static String replaceAllWhiteChars(String content) {
		content = content.replaceAll("&nbsp", "");
		content = content.replaceAll("[^a-zA-Z0-9���󜳿��]", " ");
		content = content.replaceAll("[  ]+", " ");

		return content;
	}

	public static String replaceAllUpperCaseToLowerCase(String content) {
		return content.toLowerCase();
	}

	public static Elements getAllHyperLinks() {

		Elements links = new Elements();

		List<String> lines = openFile("E:\\MyWorkspace\\PracaMagisterskaJavaLuty\\EDWILaboratorium4\\connectTheSameServer");

		// System.out.println(lines.size());
		// for (String line : lines) {
		String mainLink = GetWebPageUtils
				.processLink(MainClass.LINK_TO_WEBPAGE);
		String ipRootPage = checkAddressIP(mainLink);

		try {
			URL url = new URL(MainClass.LINK_TO_WEBPAGE);
			URLConnection con = url.openConnection();
			InputStream in = con.getInputStream();
			String encoding = con.getContentEncoding();
			encoding = encoding == null ? "UTF-8" : encoding;
			String body = IOUtils.toString(in, encoding);

			Document doc = Jsoup.parse(body);

			links = doc.select("a[href]");

			String originLink;
			String convertLink;

			List<String> linksToFirstFile = new ArrayList<String>();
			List<String> linksToSecondFile = new ArrayList<String>();

			for (Element link : links) {

				if (!link.attr("href").substring(0, 1).equals("#")
						&& !link.attr("href").contains("html")
						&& !link.attr("href").contains("mailto")) {

					originLink = link.attr("href");

					convertLink = link.attr("href");

					convertLink = GetWebPageUtils.processLink(convertLink);

					String ipCurrentPage = checkAddressIP(convertLink);

					// System.out.println(convertLink);
					// System.out.println(ipCurrentPage + " : " + ipRootPage);
					if (ipCurrentPage.equals(ipRootPage)) {
						linksToFirstFile.add(originLink);
						// System.out.println("Pierwszy plik");
						lines.add(originLink);
						// System.out.println(lines.size());
					} else {
						linksToSecondFile.add(originLink);
						// System.out.println("Drugi plik");
					}

				}

			}

			GetWebPageUtils.saveToFile(GetWebPageUtils.FIRST_FILE_NAME,
					linksToFirstFile);
			GetWebPageUtils.saveToFile(GetWebPageUtils.SECOND_FILE_NAME,
					linksToSecondFile);

			System.out.println("Zako�czono - pobieranie zawarto�ci stron");

		} catch (Exception e) {

			e.printStackTrace();
		}
		// }

		return links;
	}

	public static List<String> openFile(String path) {
		String sCurrentLine;

		List<String> lines = new ArrayList<String>();

		try (BufferedReader br = new BufferedReader(new FileReader(path
				+ ".txt"))) {

			while ((sCurrentLine = br.readLine()) != null) {

				lines.add(sCurrentLine);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

		return lines;
	}

	public static String checkAddressIP(String link) {
		InetAddress addr;
		String IP = "";

		try {
			addr = InetAddress.getByName(link);
			IP = addr.getHostAddress();

		} catch (Exception e) {

			System.err.println(link);
		}

		return IP;
	}

	public static List<String> convertAllLinks(Elements links) {
		List<String> allLinks = new ArrayList<String>();

		for (Element link : links) {
			String currentLink = link.toString();

			if (currentLink.contains("?")) {

			}

			// allLinks.add(link.toString().replace("http://", ""))
		}

		return allLinks;
	}

	public static void getContentPage() {

		try {

			String contentWebPage = GetWebPageUtils
					.getWebPage(MainClass.LINK_TO_WEBPAGE);

			GetWebPageUtils.saveContentToDedicatedFile(contentWebPage, "html");

			contentWebPage = GetWebPageUtils.openDedicatedFile("html");

			String body = GetWebPageUtils.getBodyContent(contentWebPage);

			body = GetWebPageUtils.replaceHTMLTag(body);

			body = GetWebPageUtils.replaceAllUpperCaseToLowerCase(body);

			body = GetWebPageUtils.replaceAllWhiteChars(body);

			// System.out.println(body);

			GetWebPageUtils.saveContentToDedicatedFile(body, "txt");

		} catch (MalformedURLException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}

	}
}
