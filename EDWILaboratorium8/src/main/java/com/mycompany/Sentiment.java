package com.mycompany;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

public class Sentiment {

	public static String getSentiment(String word) {

		String output = "";
		
		StringBuffer result = new StringBuffer();
		
		try {

			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpPost postRequest = new HttpPost(
					"http://text-processing.com/api/sentiment/");

			StringEntity input = new StringEntity("text=" + word);
			input.setContentType("application/x-www-form-urlencoded");
			postRequest.setEntity(input);

			HttpResponse response = httpClient.execute(postRequest);

			BufferedReader br = new BufferedReader(new InputStreamReader(
					(response.getEntity().getContent())));

			while ((output = br.readLine()) != null) {
				result.append(output);
				//System.out.println(output);

			}

			httpClient.getConnectionManager().shutdown();

		} catch (MalformedURLException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();

		}

		return result.toString();

	}

	

}