package com.mycompany;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class MainClass {

	public static String LINK_TO_WEBPAGE = "http://www.bleepingcomputer.com/forums/t/616259/no-bsod-but-several-display-crashes-and-now-ctds-while-gaming-help-please/";
	
	//public static String LINK_TO_WEBPAGE = "https://www.facebook.com/groups/159446264116600/";

	public static Integer K = 5;
	public static Integer L = 5;

	public static Integer SIZE = 250;

	public static void main(String[] args) throws MalformedURLException,
			IOException {

		Map<String, Double> negWords = new HashMap<String, Double>();
		Map<String, Double> neutralWords = new HashMap<String, Double>();
		Map<String, Double> posWords = new HashMap<String, Double>();

		String result = "";

		String content = GetWebPageUtils.getWebPage(LINK_TO_WEBPAGE);

		String body = GetContentWebPageUtils.getBodyContent(content);
		
		//GetContentWebPageUtils.getTagsP(content);

		String allWords[] = body.split(" ");

		List<String> listAllWords = Utils.getAllWords(allWords);

		System.out.println(listAllWords.size());

		String word = "";
		for (int i = 0; i < listAllWords.size(); i++) {

			word = listAllWords.get(i);

			if (word.length() < MainClass.L) {
				continue;
			}

			result = Sentiment.getSentiment(word);
			result = Utils.procceddText(result);

			result = Utils.returnLabel(result);

			String temp[] = result.split(" - ");

			if (result.contains("neutral")) {
				neutralWords.put(word, Double.parseDouble(temp[1]));
			}

			else if (result.contains("pos")) {
				posWords.put(word, Double.parseDouble(temp[1]));
			}

			else {
				negWords.put(word, Double.parseDouble(temp[1]));
			}

			System.out.println(word + " - " + result);

		}

		//SORTOWANIE
		neutralWords = Utils.sortByComparator(neutralWords);
		posWords = Utils.sortByComparator(posWords);
		negWords = Utils.sortByComparator(negWords);

		List<Double> negValues = new ArrayList<Double>(negWords.values());
		List<String> negKeys = new ArrayList<String>(negWords.keySet());

		List<Double> posValues = new ArrayList<Double>(posWords.values());
		List<String> posKeys = new ArrayList<String>(posWords.keySet());

		
		
		System.out.println("\n " + MainClass.K +  " Najbardziej negatywnych slow: ");

		Utils.printListWord(negKeys, negValues);

		System.out.println("\n " + MainClass.K +  "Najbardziej pozytywnych slow: ");

		Utils.printListWord(posKeys, posValues);

		System.out.println("");
		System.out.println("neg " + negWords.size());
		System.out.println("pos " + posWords.size());
		System.out.println("neutral " + neutralWords.size());

	}

}
