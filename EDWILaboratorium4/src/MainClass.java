import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import javax.rmi.CORBA.Util;

import org.apache.commons.io.IOUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class MainClass {

	// http://www.mkyong.com/java/jsoup-html-parser-hello-world-examples/

	private static String LINK_TO_WEBPAGE = "http://www.onet.pl/";

	public static void main(String[] args) {

		// getContentPage();
		getAllHyperLinks();

	}

	public static Elements getAllHyperLinks() {

		Elements links = new Elements();

		List<String> lines = openFile("E:\\MyWorkspace\\PracaMagisterskaJavaLuty\\EDWILaboratorium4\\connectTheSameServer");

		System.out.println(lines.size());
		//for (String line : lines) {
			String mainLink = Utils.processLink(LINK_TO_WEBPAGE);
			String ipRootPage = checkAddressIP(mainLink);

			try {
				URL url = new URL(LINK_TO_WEBPAGE);
				URLConnection con = url.openConnection();
				InputStream in = con.getInputStream();
				String encoding = con.getContentEncoding();
				encoding = encoding == null ? "UTF-8" : encoding;
				String body = IOUtils.toString(in, encoding);

				Document doc = Jsoup.parse(body);

				links = doc.select("a[href]");

				String originLink;
				String convertLink;

				List<String> linksToFirstFile = new ArrayList<String>();
				List<String> linksToSecondFile = new ArrayList<String>();

				for (Element link : links) {

					if (!link.attr("href").substring(0, 1).equals("#")
							&& !link.attr("href").contains("html")
							&& !link.attr("href").contains("mailto")) {

						originLink = link.attr("href");

						convertLink = link.attr("href");

						convertLink = Utils.processLink(convertLink);

						String ipCurrentPage = checkAddressIP(convertLink);

						// System.out.println(convertLink);
						System.out.println(ipCurrentPage + " : " + ipRootPage);
						if (ipCurrentPage.equals(ipRootPage)) {
							linksToFirstFile.add(originLink);
							System.out.println("Pierwszy plik");
							lines.add(originLink);
							System.out.println(lines.size());
						} else {
							linksToSecondFile.add(originLink);
							System.out.println("Drugi plik");
						}

					}

				}

				Utils.saveToFile(Utils.FIRST_FILE_NAME, linksToFirstFile);
				Utils.saveToFile(Utils.SECOND_FILE_NAME, linksToSecondFile);

				System.out.println("Zakończono");

			} catch (Exception e) {

				e.printStackTrace();
			}
		//}

		return links;
	}

	public static List<String> openFile(String path) {
		String sCurrentLine;

		List<String> lines = new ArrayList<String>();

		try (BufferedReader br = new BufferedReader(new FileReader(path
				+ ".txt"))) {

			while ((sCurrentLine = br.readLine()) != null) {

				lines.add(sCurrentLine);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

		return lines;
	}

	public static String checkAddressIP(String link) {
		InetAddress addr;
		String IP = "";

		try {
			addr = InetAddress.getByName(link);
			IP = addr.getHostAddress();

		} catch (Exception e) {

			System.err.println(link);
		}

		return IP;
	}

	public static List<String> convertAllLinks(Elements links) {
		List<String> allLinks = new ArrayList<String>();

		for (Element link : links) {
			String currentLink = link.toString();

			if (currentLink.contains("?")) {

			}

			// allLinks.add(link.toString().replace("http://", ""))
		}

		return allLinks;
	}

	public static void getContentPage() {

		try {

			String contentWebPage = Utils.getWebPage(LINK_TO_WEBPAGE);

			Utils.saveContentToDedicatedFile(contentWebPage, "html");

			contentWebPage = Utils.openDedicatedFile("html");

			String body = Utils.getBodyContent(contentWebPage);

			body = Utils.replaceHTMLTag(body);

			body = Utils.replaceAllUpperCaseToLowerCase(body);

			body = Utils.replaceAllWhiteChars(body);

			System.out.println(body);

			Utils.saveContentToDedicatedFile(body, "txt");

		} catch (MalformedURLException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}

	}
}
