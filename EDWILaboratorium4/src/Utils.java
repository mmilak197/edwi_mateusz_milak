import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class Utils {

	private static String NAME_FILE_WITH_DOT = "index.";
	public static String FIRST_FILE_NAME = "connectTheSameServer";
	public static String SECOND_FILE_NAME = "connectToOtherServer";

	public static void saveToFile(String nameFile, List<String> allLinks) {
		File file = new File(nameFile + ".txt");

		StringBuffer contentToSave = new StringBuffer();

		for (String s : allLinks) {
			contentToSave.append(s + "\n");
		}

		try {
			if (!file.exists()) {
				file.createNewFile();
			}

			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(contentToSave.toString());
			bw.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static String processLink(String convertLink) {
		convertLink = convertLink.replace("http://", "")
				.replace("https://", "");

		if (convertLink.contains("/")) {
			int index = convertLink.indexOf("/");

			convertLink = convertLink.substring(0, index + 1);
		}

		if (convertLink.contains("?")) {
			int index = convertLink.indexOf("?");

			convertLink = convertLink.substring(0, index);
		}

		return convertLink = convertLink.replaceAll("/", "");
	}

	public static String getWebPage(String linkToWebPage)
			throws MalformedURLException, IOException {

		URL url = new URL(linkToWebPage);
		URLConnection con = url.openConnection();
		InputStream in = con.getInputStream();
		String encoding = con.getContentEncoding();
		encoding = encoding == null ? "UTF-8" : encoding;
		String page = IOUtils.toString(in, encoding);

		return page;
	}

	public static void saveContentToDedicatedFile(String contentWebPage,
			String extension) {

		File file = new File(NAME_FILE_WITH_DOT + extension);

		try {
			if (!file.exists()) {
				file.createNewFile();
			}

			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(contentWebPage);
			bw.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static String openDedicatedFile(String extension) {

		String sCurrentLine;
		StringBuffer allContentFile = new StringBuffer();

		try (BufferedReader br = new BufferedReader(new FileReader(
				NAME_FILE_WITH_DOT + extension))) {

			while ((sCurrentLine = br.readLine()) != null) {

				allContentFile.append(sCurrentLine + "\n");
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

		return allContentFile.toString();
	}

	public static String getBodyContent(String contentWebPage) {

		Document doc = Jsoup.parse(contentWebPage);
		String body = doc.body().text();

		return body;
	}

	public static String replaceHTMLTag(String bodyContentWebPage) {

		String temp = bodyContentWebPage.replaceAll("\\<.*?>", "");

		temp = temp.replaceAll("<!--", "").replaceAll("-->", "");

		return temp;
	}

	public static String replaceAllWhiteChars(String content) {
		content = content.replaceAll("&nbsp", "");
		content = content.replaceAll("[^a-zA-Z0-9���󜳿��]", " ");
		content = content.replaceAll("[  ]+", " ");

		return content;
	}

	public static String replaceAllUpperCaseToLowerCase(String content) {
		return content.toLowerCase();
	}
}
